main() {
    link ../week01/file.txt _ex2.txt

    inode_num="$(ls -i _ex2.txt | awk '{print $1}')"
    echo "All links to ${inode_num}"
    find -inum ${inode_num}
    find -inum ${inode_num} -exec rm {} \; 
}

main > ex2.txt
