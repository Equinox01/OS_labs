# Linking files
ln _ex1.txt _ex1_1.txt
ln _ex1.txt _ex1_2.txt

# Writing i-node numbers to the file
ls -i _ex1.txt > ex1.txt
ls -i _ex1_1.txt >> ex1.txt
ls -i _ex1_2.txt >> ex1.txt

# Remove links
rm _ex1_1.txt
rm _ex1_2.txt

echo Done