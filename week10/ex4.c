#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

void print_all_linked(char* dir, ino_t inode_num, FILE *fp);

void main(int argc, char** argv){
    FILE *fp;
    char* dir_relative;
    char dir_abs[256];
    DIR *tmp;
    struct dirent *curr;
    struct stat *file_stat = malloc(sizeof(struct stat));
    int links_cnt = 0;
    
    if (argc > 2){
        printf("Only 1 argument is requred\n");
        return;
    }

    fp = fopen("ex4.txt", "w");
    if (fp == NULL){
        printf("Can't open file 'ex4.txt'");
    }

    if (argc == 1){
        dir_relative = ".";
    } else {
        dir_relative = argv[1];
    }
    realpath(dir_relative ,dir_abs);

    if ((tmp = opendir(dir_abs)) != NULL){
        while ((curr = readdir(tmp)) != NULL){
            stat(curr->d_name ,file_stat);
            fprintf(fp, "File: %s\n", curr->d_name);
            fprintf(fp, "Hard links: %u\n", (file_stat->st_nlink));
            if (file_stat->st_nlink > 1){
                // print_all_linked("/", file_stat->st_ino);
                print_all_linked(dir_abs, file_stat->st_ino, fp);
            }
        }
    } else {
        printf("Directory \"%s\" not found\n", dir_abs);
    }

    closedir(tmp);
    fclose(fp);
}

void print_all_linked(char* dir, ino_t inode_num, FILE* fp){
    /**
     * Print all hard links to the file with specific i-node number from specific directory
    **/
    DIR *tmp;
    struct dirent *curr;
    struct stat *file_stat = malloc(sizeof(struct stat));
    char* temp_dir = malloc(256 * sizeof(char));
    if ((tmp = opendir(dir)) != NULL){
        while ((curr = readdir(tmp)) != NULL){
            stat(curr->d_name ,file_stat);
            if (file_stat->st_ino == inode_num){
                fprintf(fp, " - %s\n", curr->d_name);
            }
            if (!strcmp(curr->d_name, ".\0")){
                continue;
            } else if (!strcmp(curr->d_name, "..\0")){
                continue;
            }
            strcpy(temp_dir, dir);
            strcat(temp_dir, "/");
            strcat(temp_dir, curr->d_name);
        }
    }
    free(temp_dir);
    free(file_stat);
    closedir(tmp);
}