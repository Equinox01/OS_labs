#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#define BUFF_SIZE 64    // Lower buffer size for more instant result (Can be boring :( )
#define MOD 100

int count = 0;
int status = 1;
int consumer_sleep = 0;
int producer_sleep = 0;

void awake_consumer(){
    // Here should be signals, but it is a secret :)
    int prev_value = consumer_sleep;
    consumer_sleep = 0;
    consumer_sleep = prev_value;
    printf("Awake signal to consumer was sent\n");
}

void awake_producer(){
    // Here should be signals, but it is a secret :)
    int prev_value = producer_sleep;
    producer_sleep = 0;
    producer_sleep = prev_value;
    printf("Awake signal to producer was sent\n");
}

void producer_funct(int* buffer){
    /**
     * Adds to the buffer random number in range (0-100)
     * */
    int sleep_output = 0;
    int new_number;
    printf("Producer successfully started!\n");
    while (status){
        if (count < BUFF_SIZE){
            new_number = rand() % MOD;
            buffer[count++] = new_number;
            printf("added [%d]\n", count);
            awake_consumer();
        } else {
            // sleep_producer();
            producer_sleep = 1;
            while (producer_sleep){
                if (!sleep_output){
                    printf("Producer is sleeping!\n");
                }
                sleep_output = 1;
                if (status == 0){
                    pthread_exit(NULL);
                }
            }
            printf("Producer is awake!\n");
            sleep_output = 0;

        }
    }
    printf("Producer successfully finished!\n");
    pthread_exit(NULL);
}

void consumer_funct(int* buffer){
    /**
     * Pops integer from a buffer and return its square
     **/
    int sleep_output = 0;
    int current;
    printf("Consumer successfully started!\n");
    while (status){
        if (count > 0){
            current = buffer[count-1];
            count--;
            printf("removed [%d]\n", count+1);
            awake_producer();
            printf ("%d ^ 2 = %d\n", current, current*current);
        } else {
            // sleep_consumer()
            consumer_sleep = 1;
            while (consumer_sleep){
                if (!sleep_output){
                    printf("Consumer is sleeping!\n");
                }
                sleep_output = 1;
                if (status == 0){
                    pthread_exit(NULL);
                }
            }
            printf("Consumer is awake!\n");
            sleep_output = 0;
        }
    }
    printf("Consumer successfully finished!\n");
    pthread_exit(NULL);
}

void main(){
    int* buffer = malloc(64 * sizeof(int));
    pthread_t producer;
    pthread_t consumer;
    srand(time(NULL));

    pthread_create(&producer, NULL, &producer_funct, (void*)buffer);
    pthread_create(&consumer, NULL, &consumer_funct, (void*)buffer);

    // sleep(60);   // Gives more interesting results but doesn't indicate when racing condition is reached

    // This code works, but can be choosen by scheduler and don't let consumer/producer proceed (in case of CPU running only 2 threads in parallel)
    while (1){
        sleep(2);
        if (consumer_sleep && producer_sleep){
            status = 0;
            break;
        }
    }
    printf("RACING CONDITION WAS REACHED\n");
    
    return;
    // pthread_join(&consumer, NULL);
    // pthread_join(&consumer, NULL);
}