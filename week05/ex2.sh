#!/bin/bash

filename='ex2_numbers.txt'
function main {
    local times=5;
    for ((idx=0;idx<$times;idx++)); do
        while [ -f $filename.lock ]; do
            echo -n "";
        done
        ln $filename $filename.lock
        # Critical region starts here
        local n=$(tail -n 1 ex2_numbers.txt);
        n=$[n+1];
        echo $n;
        echo $n >> $filename;
        # Critical region ends here
        rm $filename.lock;
        sleep 0.01
    done
}

if [ -e $filename ]; then
    rm $filename;
fi
touch $filename;
echo 0 > $filename;

(main) & (main)

wait