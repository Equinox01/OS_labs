#include <pthread.h>

void funct(int n){
    printf("I, thread #%d, was succsesfully created\n", n);
    pthread_exit(NULL);
    return;
}

void main(){
    int i;
    int n = 5;
    int wait_to_join = 1;
    pthread_t* threads = malloc(sizeof(pthread_t) * n);
    for (i=0;i<n;i++){
        pthread_create(&threads[i], NULL, funct, (void*)i);
        printf("Thread %d was succsesfully created\n", i);
        if (wait_to_join){
            pthread_join(threads[i], NULL);
        }
    }
    if (!wait_to_join){
        for (i=0;i<n;i++){
            pthread_join(threads[i], NULL);
        }
    }
    printf("Done!\n");
    return;
}
