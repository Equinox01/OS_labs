#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/fcntl.h>

void main(){
    FILE *fp;
    struct stat *file_info = malloc(sizeof(struct stat));
    int len;

    // Get file length
    fp = open("ex1.txt", O_RDWR);
    // fp = open("ex1.txt", O_RDONLY);
    // fp = open("ex1.txt", 'r+');
    if (fp == -1){
        printf("Failed to open");
        return;
    }
    if (fstat(fp, file_info) < 0){
        printf("Fstat failed");
        close(fp);
        return;
    }
    len = file_info->st_size;
    void *mapped_space = malloc(len);
    printf("len: %d\n", len);
    printf("inod_n: %d\n", file_info->st_ino);
    
    mapped_space = mmap(mapped_space, len, PROT_WRITE | PROT_READ, MAP_SHARED, fp, 0);
    printf("maped_space before str_cpy: %s\n", mapped_space);
    strcpy(mapped_space, "This is a nice day!");
    printf("maped_space: %s\n", mapped_space);
    
    if (msync(mapped_space, len, MS_SYNC) != 0){
        printf("msync failed!\n");
    }
    munmap(mapped_space, len);
    close(fp);
    printf("Done\n");
}