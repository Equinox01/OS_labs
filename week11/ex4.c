#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/fcntl.h>

void main(){
    FILE *fp_read, *fp_write;
    struct stat *file_info = malloc(sizeof(struct stat));
    int len;
    char file_to_read[] = "ex4.txt";
    char file_to_write[] = "ex4.memcpy.txt";
    void *mapped_space_read;
    void *mapped_space_write;
    void *temporary_storage;

    // Open file for read and read its content
    fp_read = open(file_to_read, O_RDONLY);

    if (fp_read == -1){
        printf("Failed to open file to read");
        return;
    }
    if (fstat(fp_read, file_info) < 0){
        printf("Fstat failed");
        close(fp_read);
        return;
    }

    len = file_info->st_size;
    mapped_space_read = mmap(NULL, len, PROT_READ, MAP_SHARED, fp_read, 0);

    printf("len: %d\n", len);
    printf("inod_n: %d\n", file_info->st_ino);

    // Place file's content to the temporary storage
    temporary_storage = malloc(len);
    memcpy(temporary_storage, mapped_space_read, len);

    // Close file and unmap
    munmap(mapped_space_read, len);
    close(fp_read);

    printf("Content of file: %s\n", temporary_storage);

    // Open file to write
    fp_write = open(file_to_write, O_RDWR);
    ftruncate(fp_write, len);   // Truncate to needed size

    if (fp_write == -1){
        printf("Failed to open file to write");
        return;
    }

    mapped_space_write = mmap(NULL, len, PROT_WRITE, MAP_SHARED, fp_write, 0);

    // Write to mapped space
    memcpy(mapped_space_write, temporary_storage, len);
    
    // Update file
    if (msync(mapped_space_write, len, MS_SYNC) != 0){
        printf("msync failed!\n");
    }

    // Unmap and close file
    munmap(mapped_space_write, len);
    close(fp_write);

    printf("Done\n");
}