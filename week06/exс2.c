#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

void main(){
    char* s1 = "String to transfer";
    char* s2 = malloc(strlen(s1) + 1);
    int len;
    int fp[2];
    if (pipe(fp) < 0){
        printf("pipe() is not working :(");
        return;
    }
    int len_str = (int)strlen(s1) + 1;
    printf("Write response: %d\n", write(fp[1], &len_str, sizeof(int)));
    printf("Write response: %d\n", write(fp[1], s1, strlen(s1) + 1));
    if (fork() == 0){
        len = malloc(sizeof(int));
        printf("Read response: %d\n", read(fp[0], &len, sizeof(int)));
        printf("Length = %d\n", len);
        printf("Read response: %d\n", read(fp[0], s2, len));
        printf("Result: %s\n", s2);
    }
}