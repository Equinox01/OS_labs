#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void sigkill_handler(int sig){
    printf("\nSIGKILL signal received. Initializing self-destruction of the process...\n");
}

void sigstop_handler(int sig){
    printf("\nSIGSTOP signal received. Take your time, I am waiting.\n");
}

void sigusr1_handler(int sig){
    printf("\nSIGUSR1 signal received. I will stop here. Bye.\n");
    exit(1);
}

void main(){

    printf("Process is started. My PID is %d\n", getpid());

    signal(SIGKILL, &sigkill_handler);
    signal(SIGSTOP, &sigstop_handler);
    signal(SIGUSR1, &sigusr1_handler);

    sleep(60);
}