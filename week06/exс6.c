#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>

int flag = 0;

void sigusr1_handler(int sig){
    printf("SIGUSR1 signal was received by process %d.\n", getpid());
    flag = 1;
}

void main(){

    int fp[2];
    int first_child_pid;
    int second_child_pid;
    int second_child_status;

    printf("Parent process is started with PID %d\n", getpid());
    signal(SIGUSR1, &sigusr1_handler);
    pipe(fp);

    if ((first_child_pid=fork()) == 0){
        printf("First child process started with PID %d\n", getpid());
        while (!flag);
        printf("It will be done. (Will send SIGSTOP to other child in 2 seconds)\n");
        sleep(2);
        read(fp[0], &second_child_pid, sizeof(int));
        kill(second_child_pid, SIGSTOP);
        exit(0);
    }

    if ((second_child_pid=fork()) == 0){
        printf("Second child process started with PID %d\n", getpid());
        second_child_pid = getpid();
        write(fp[1], &second_child_pid, sizeof(int));
        printf("Sending SIGUSR1 to %d\n", getppid());
        kill(getppid(), SIGUSR1);
        while(1);
    }

    while(!flag);
    printf("Execute order 66. (Sending signal from parent to first child)\n");
    kill(first_child_pid, SIGUSR1);
    waitpid(second_child_pid, &second_child_status, WUNTRACED);
    printf("Second child exit with status code %d.\n", second_child_status);
    return;
}