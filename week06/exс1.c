#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

void main(){
    char* s1 = "String to transfer";
    char* s2 = malloc(strlen(s1) + 1);
    // char* s2 = "Wrong string here!";
    int fp[2];
    if (pipe(fp) < 0){
        printf("pipe() is not working :(");
        return;
    }
    // printf("Write response: %d\n", write(fp[1], s1, (strlen(s1)+1) * sizeof(char)));
    printf("Write response: %d\n", write(fp[1], &s1, 4));
    // printf("%d\n", fp[1]);
    // printf("Read response: %d\n", read(fp[0], s2, (strlen(s1)+1) * sizeof(char)));
    printf("Read response: %d\n", read(fp[0], &s2, 4));
    // printf("%d\n", fp[0]);
    // *(s2 + 2) = 'a';    // <- Segfault here
    // printf("s1: %s\n", s1);
    // printf("s2: %s\n", s2);
    printf("Result: %s\n", s2);
}