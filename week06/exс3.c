#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void sigint_handler(int sig){
    printf("\nSIGINT signal received. Initializing self-destruction of the process...\n");
}

void main(){

    printf("Process is started. My PID is %d\n", getpid());
    signal(SIGINT, &sigint_handler);

    sleep(60);
}