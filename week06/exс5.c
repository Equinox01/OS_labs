#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void sigterm_handler(int sig){
    printf("\nSIGTERM signal was received.\n");
    exit(0);
}

void main(){

    printf("Parent process is started. My PID is %d\n", getpid());

    int child_pid;

    if ((child_pid = fork()) == 0){
        signal(SIGTERM, &sigterm_handler);
        while(1){
            printf("I am alive!\n");
            sleep(1);
        }
    } else {
        printf("Child PID: %d\n", child_pid);
        sleep(10);
        kill(child_pid, SIGTERM);
    }
}