#include <stdlib.h>
#include <stdio.h>
#include <sys/fcntl.h>
#include <string.h>
#include <unistd.h>

void main(int argc, char** argv){

    int add = 0;
    int i;
    int rights = O_WRONLY | O_CREAT | O_TRUNC;
    char temp;
    FILE** fp;
    if (argc >= 2){
        if (!strcmp(argv[1], "-a")){
            add = 1;
            rights = O_APPEND | O_WRONLY;
        }
    }
    if (argc <= 1 + add){
        printf("No files defined\n");
        return;
    }
    fp = malloc(sizeof(FILE*) * (argc - add));
    for (i = 1 + add; i < argc; i++){
        if ((fp[i - 1 - add] = open(argv[i], rights)) == NULL){
            fprintf(stdout, "Failed to open %s\n", argv[i]);
            return;
        }
    }

    while ((temp = fgetc(stdin)) != EOF){
        write(1, &temp, 1); // Write to stdout
        // fprintf(stdout, "%c", temp);
        for (i = 0; i < argc - 1 - add; i++){
            write(fp[i], &temp, 1);
        }
    }

    fflush(stdout);

    for (i = 0; i < argc - 1 - add; i++){
        close(fp[i]);
    }
    
}