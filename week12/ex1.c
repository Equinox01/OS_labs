#include <stdlib.h>
#include <stdio.h>
#include <sys/fcntl.h>

void main(){
    int k = 20;
    char *s = malloc(k);
    FILE *random_fp = open("/dev/random", O_RDONLY);
    read(random_fp, s, k);
    printf("%s\n", s);
    close(random_fp);
}