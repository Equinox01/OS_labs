#include <stdio.h>

struct Node {
    int content;
    struct Node *next;
    struct Node *prev;
    struct LinkedList *root;
};

struct LinkedList {
    struct Node *head;
    struct Node *tail;
};

void print_list(struct LinkedList *list){
    struct Node *curr = list->head;
    while (curr != NULL){
        printf("%d ", curr->content);
        curr = curr->next;
    }
    printf("\n");
}

int insert_node(struct LinkedList *list, int index, int content){
    /**Insert after element with specified index. Cannot insert in the head. Indexes start from 1.
     **/
    struct Node *node;
    struct Node *curr = list->head;
    if (curr == NULL || index == 0){
        return (-1);
    }
    while (--index > 0){
        curr = curr->next;
        if (curr == NULL){
            return(-1); // Out of bound
        }
    }
    node = malloc(sizeof(struct Node));
    node->content = content;
    node->root = list;
    node->next = curr->next;
    node->prev = curr;
    curr->next = node;
    if (node->next != NULL){
        node->next->prev = node;
    } else {
        list->tail = node;
    }
    return(0);
}

int insert_node_tail(struct LinkedList *list, int content) {
    /**Insert to the end of the linked list
     **/
    struct Node *curr = malloc(sizeof(struct Node));
    curr->content = content;
    curr->root = list;
    curr->prev = NULL;
    curr->next = NULL;
    if (list->head == NULL){
        list->head = curr;
        list->tail = curr;
        return (0);
    }
    curr->prev = list->tail;
    list->tail->next = curr;
    list->tail = curr;
    return(0);
}

int insert_node_head(struct LinkedList *list, int content) {
    /**Insert to the beginning of the linked list
     **/
    struct Node *curr = malloc(sizeof(struct Node));
    curr->content = content;
    curr->root = list;
    curr->prev = NULL;
    curr->next = NULL;
    if (list->head == NULL){
        list->head = curr;
        list->tail = curr;
        return (0);
    }
    curr->next = list->head;
    list->head->prev = curr;
    list->head = curr;
    return(0);
}

int delete_node(struct Node *node){
    /**Node to delete should be passed
    **/
    if (node == node->root->head){
        node->root->head = node->next;
    }
    if (node == node->root->tail){
        node->root->tail = node->prev;
    }
    if (node->next != NULL){
        node->next->prev = node->prev;
    }
    if (node->prev != NULL){
        node->prev->next = node->next;
    }
    free(node);
}

int main(){
    struct LinkedList ll = {
        .head=NULL,
        .tail=NULL
    };
    printf("Insert 1, 2 and 3\n");
    insert_node_tail(&ll, 1);
    insert_node_tail(&ll, 2);
    insert_node_tail(&ll, 3);
    print_list(&ll);
    printf("Insert 0 and -1 to the beginning\n");
    insert_node_head(&ll, 0);
    insert_node_head(&ll, -1);
    print_list(&ll);
    printf("Delete 2 nodes from the tail\n");
    delete_node(ll.head);
    print_list(&ll);
    delete_node(ll.head);
    print_list(&ll);
    printf("Delete node from the beginning\n");
    delete_node(ll.tail);
    print_list(&ll);
    printf("Clear linked list\n");
    delete_node(ll.head);
    print_list(&ll);
    delete_node(ll.head);
    print_list(&ll);
    printf("Insert 1, 2 and 3\n");
    insert_node_tail(&ll, 1);
    insert_node_tail(&ll, 2);
    insert_node_tail(&ll, 3);
    print_list(&ll);
    printf("Delete node from middle\n");
    delete_node(ll.head->next);
    print_list(&ll);
    printf("Insert after element with specified index\n");
    insert_node(&ll, 1, 2);
    print_list(&ll);
    printf("Insert after last element\n");
    insert_node(&ll, 3, 4);
    print_list(&ll);
    printf("Insert after node with index out of bound (should not insert anything)\n");
    insert_node(&ll, 5, 100);
    print_list(&ll);
    return(0);
}