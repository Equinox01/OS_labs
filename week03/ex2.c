#include <stdio.h>
#include <string.h>

int* bubble_sort(int n, int *a){
    int i = 0, j = 0, temp = 0;
    for (i=0;i<n;i++){
        for (j=i;j<n;j++){
            if (a[i] > a[j]){
                temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
    }
    return(a);
}

int main(){
    int n;
    int *arr;
    int i;
    int temp;
    printf("Want to see some trick?\nEnter your array's size:\n");
    scanf("%d", &n);
    printf("Enter your array:\n");
    arr = malloc(sizeof(int) * n);
    for (i=0;i<n;i++){
        scanf("%d", &temp);
        arr[i] = temp;
    }   
    bubble_sort(n, arr);
    printf("Here is your sorted array:\n");
    for (i=0;i<n-1;i++){
        printf("%d, ", arr[i]);
    }
    printf("%d\n", arr[n-1]);
}

