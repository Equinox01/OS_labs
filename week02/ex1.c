#include <limits.h>
#include <float.h>
#include <stdio.h>

int main(){
    int var_int = INT_MAX;
    float var_float = FLT_MAX;
    double var_double = DBL_MAX;

    printf("int: size=%d, max value=%d\n", (int)sizeof(int), var_int);
    printf("float: size=%d, max value=%f\n", (int)sizeof(float), var_float);
    printf("double: size=%d, max value=%f\n", (int)sizeof(double), var_double);

    return(0);
}