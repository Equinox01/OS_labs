#include <stdio.h>

void swap_it(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

int main(){
    int a=0,b=0;
    printf("I would be grateful if you give me 2 integers\n");
    scanf("%d %d", &a, &b);
    swap_it(&a, &b);
    printf("Here are your integers: %d %d\n", a, b);
    return(0);
}