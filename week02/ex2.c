#include <stdio.h>
#include <string.h>

char *read_str(FILE *fp){
    int len = (int)sizeof(char);
    int pt = 0;
    char *str = malloc(len);
    char temp;
    while (EOF != (temp=fgetc(fp)) && temp != '\n'){
        str = realloc(str, len + sizeof(char));
        str[pt++] = temp;
        len += sizeof(char);
    }
    return(str);
}

int main(){
    char *str;
    int str_size = 0;
    int i = 0;
    str = read_str(stdin);
    str_size = strlen(str);
    for (i=str_size-1;i>=0;--i){
        putchar(str[i]);
    }
    putchar('\n');
    return (0);
}