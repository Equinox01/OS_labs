#include <stdio.h>

int main(int argc, char** args){
    int n=0;
    int i=0,j=0;
    int width=0;
    char *buff;
    sscanf(args[1], "%d", &n);
    // printf("%s\n", args[1]);
    width = n*2 -1;
    // printf("%d\n", width);
    buff = malloc(width * sizeof(char));
    memset(buff, ' ', width);
    // printf("%d\n", 2);
    for (i=0;i<n;i++){
        buff [width/2 + i] = '*';
        buff [width/2 - i] = '*';
        printf("%s\n", buff);
    }

    return(0);
}