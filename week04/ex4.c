#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#define DEBUG 0

char *read_str(FILE *fp){
    int len = (int)sizeof(char);
    int pt = 0;
    char *str = malloc(len);
    char temp;
    while (EOF != (temp=fgetc(fp)) && temp != '\n'){
        str = realloc(str, len + sizeof(char));
        str[pt++] = temp;
        len += sizeof(char);
    }
    str = realloc(str, len + sizeof(char));
    str[pt++] = '\0';
    return(str);
}

char* strip(char* inp){
    int idx = 0;
    int start=0, finish=strlen(inp)-1;
    char* str;
    // Strip
    for (idx=0;idx<finish;idx++){
        if (inp[idx] == ' '){
            start++;
        } else {
            break;
        }
    }
    for (idx=finish;idx>=0;idx--){
        if (inp[idx] == ' '){
            finish--;
        } else {
            break;
        }
    }
    str = malloc(finish-start+2);
    for (idx=start;idx<=finish;idx++){
        str[idx-start] = inp[idx];
    }
    str[finish+1] = '\0';
    // Pring result
    if (DEBUG){
        printf("Stripped: '%s'\n", str);
    }
    return(str);
}

char** process_args(char* inp){
    int idx = 0;
    int curr_idx = 0;
    int argc = 1;
    int space = 0;
    char* str;
    char* curr_arg = malloc(sizeof(char));
    char** args = malloc(sizeof(char*));
    args[0] = curr_arg;
    // Process
    str = strip(inp);
    idx = 0;
    while (str[idx] != '\0'){
        if (str[idx] == ' '){
            if (!space){
                curr_arg = realloc(curr_arg, curr_idx + 1);
                curr_arg[curr_idx] = '\0';
                argc++;
                args = realloc(args, sizeof(char*) * (argc));
                curr_arg = malloc(sizeof(char));
                args[argc-1] = curr_arg;
                curr_idx = 0;
                // printf("!\n");
            }
            idx++;
            space = 1;
            // printf("?\n");
            continue;
        }
        space = 0;
        // if (curr_idx > 0){
        curr_arg = realloc(curr_arg, curr_idx + 1);
        // }
        // printf("%c\n", str[idx]);
        curr_arg[curr_idx] = str[idx];
        // printf("%c\n", curr_arg[curr_idx]);
        curr_idx++;
        idx++;
    }
    // Add NULL to the end
    args = realloc(args, sizeof(char*) * (++argc));
    args[argc] = NULL;
    // Print args
    if (DEBUG){
        printf("args: ");
        for (curr_idx = 0;curr_idx<argc;curr_idx++){
            printf("'%s', ", args[curr_idx]);
        }
        printf("\n");
    }
    // free str since it will be dangling pointer
    // Doesn't work with input "a b" :D
    // free(str);
    return(args);
}

int main(){
    while(1){
        char* input;
        char** args;
        input = read_str(stdin);
        if (strcmp(input, "exit") == 0){
            return(0);
        }
        args = process_args(input);
        if (fork() == 0){
            if (execvp(args[0], args) == -1){
                printf("Command '%s' doesn't exist or exited with error\n", input);
            }
            exit(0);
        }
        free(input);
    }
}

