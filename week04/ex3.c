#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

char *read_str(FILE *fp){
    int len = (int)sizeof(char);
    int pt = 0;
    char *str = malloc(len);
    char temp;
    while (EOF != (temp=fgetc(fp)) && temp != '\n'){
        str = realloc(str, len + sizeof(char));
        str[pt++] = temp;
        len += sizeof(char);
    }
    return(str);
}

int main(){
    while(1){
        char* input;
        input = read_str(stdin);
        if (strcmp(input, "exit") == 0){
            return(0);
        }
        // if (fork() == 0){
            if (execlp(input, input, (char*)NULL) == -1){
                printf("Command '%s' doesn't exist or have parameters to process\n", input);
            }
        //     exit(0);
        // }
        free(input);
    }
}

