#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(){
    int n;
    int rv;
    if (fork() == 0){
        printf("Hello from child [%d]\n", (int)getpid() - n);
    } else {
        printf("Hello from parent [%d]\n", (int)getpid() - n);
    }
    printf("\n");
    return(0);
}