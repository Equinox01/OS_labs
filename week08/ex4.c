#include <sys/resource.h>
#define TOTAL_MEMORY ( (size*(i+1) + sizeof(int) * 2 + sizeof(void*))/1024 )
#define DEBUG 1

void main(){
    int i;
    struct rusage* rs;
    void* lot_of_memory;
    const int size = 10 * 1024 * 1024;
    for (i=0;i<10;i++){
        rs = malloc(sizeof(struct rusage));
        lot_of_memory = malloc(size);
        memset(lot_of_memory, 0, size);
        getrusage(RUSAGE_SELF, rs);
        if (DEBUG){
            printf("Memory usage: %d (should be %d, difference %d)\n", rs->ru_maxrss, TOTAL_MEMORY, rs->ru_maxrss - TOTAL_MEMORY);
        } else {
            printf("Memory usage: %d\n", rs->ru_maxrss);
        }
        free(rs);
        sleep(1);
    }
}