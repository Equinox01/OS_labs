#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void* my_realloc(void* object, size_t size);

void print_array(int* a, int n){
    int i;
    for (i=0;i<n/sizeof(int);i++){
        printf("%d ", a[i]);
    }
    printf("\n");
}

void main(){
    int i;
    int n = 5 * sizeof(int);
    int *a = malloc(n);
    a[0] = 1;
    a[1] = 2;
    a[2] = 3;
    a[3] = 4;
    a[4] = 5;
    printf("Allocating new array of integers {1,2,3,4,5}\n");
    print_array(a,n);
    n = 7 * sizeof(int);
    printf("Realloc to size 7\n");
    a = my_realloc(a, n);
    print_array(a,n);
    n = 3 * sizeof(int);
    printf("Realloc to size 3\n");
    a = my_realloc(a, n);
    print_array(a,n);
    printf("Realloc to size 0 (Should work like free())\n");
    a = my_realloc(a, 0);   // Should work like free()
    n = 5 * sizeof(int);
    a = my_realloc(a, n);   // Should work like malloc()
    a[0] = 1;
    a[1] = 2;
    a[2] = 3;
    a[3] = 4;
    a[4] = 5;
    printf("Realloc to size 5 (Should work like malloc()) and fill with 1,2,3,4,5\n");
    print_array(a,n);
}

void* my_realloc(void* object, size_t size){
    int i;
    char* ptr;
    if (size == 0){
        if (object != NULL){
            free(object);
        }
        return (NULL);
    }
    if (object == NULL){
        return (malloc(size));
    }
    void* temp = malloc(size);
    // memcpy(temp, object, size);
    for (i=0;i<size;i++){
        ptr = ((char*)temp) + i;
        *ptr = *(((char*)object) + i);
    }
    return (temp);
}