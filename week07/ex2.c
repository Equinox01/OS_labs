#include <stdlib.h>
#include <stdio.h>

void main(){
    int n;
    int i;
    printf("Enter the size of the array:\n");
    scanf("%d", &n);
    printf("N = %d\n", n);
    int* a = malloc(n*sizeof(int));
    printf("Filling array\n");
    for (i=0;i<n;i++){
        a[i] = i;
    }
    printf("Array's content:\n");
    for (i=0;i<n;i++){
        printf("%d ", a[i]);
    }
    printf("\n");
    free(a);
    printf("Done\n");
}