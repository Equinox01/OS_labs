#include <stdio.h>

int main() {
    char **s = malloc(sizeof(char**));
    // printf("1\n");
    char foo[] = "Hello World";
    // printf("2\n");
    *s = foo;
    // printf("3\n");
    printf("s is %s\n",*s);
    // printf("4\n");
    s[0] = foo;
    // printf("5\n");
    printf("s[0] is %s\n",s[0]);
    // printf("6\n");
    return(0);
}