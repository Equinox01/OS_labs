#include <stdlib.h>
#include <stdio.h>

#define DEBUG 1

struct page {
    int page_id;
    int r;
    unsigned char cnt;
};

void print_bin(char x){
    if (x & 0x80) printf("1"); else printf("0");
    if (x & 0x40) printf("1"); else printf("0");
    if (x & 0x20) printf("1"); else printf("0");
    if (x & 0x10) printf("1"); else printf("0");
    if (x & 0x8) printf("1"); else printf("0");
    if (x & 0x4) printf("1"); else printf("0");
    if (x & 0x2) printf("1"); else printf("0");
    if (x & 0x1) printf("1"); else printf("0");
}

void main(int argc, char **argv){
    FILE *fp;
    int page_count;
    int current_page;
    int is_hit = 0;
    int hit = 0, miss = 0;
    int clk = 0;
    int page_to_replace;
    char temp;
    struct page **pages;
    int i;

    if (argc != 2){
        printf("Args received: %d, should be 1", argc);
        return;
    }

    page_count = atoi(argv[1]);
    // printf("%d\n", page_count);
    // pages = malloc(page_count * sizeof(struct page));    // Doesn't work :(
    pages = malloc(page_count * sizeof(struct page*));
    for (i=0;i<page_count;i++){
        pages[i] = malloc(sizeof(struct page));
        pages[i]->cnt = 0x0;
        pages[i]->r = 0;
        pages[i]->page_id = -1;
    }
    
    fp = fopen("Lab 09 input.txt", "r");

    while ((temp = fgetc(fp)) != EOF){
        ungetc(temp, fp);
        fscanf(fp, "%d", &current_page);
        // printf("%d\n", current_page);
        while ((temp = fgetc(fp)) == ' ' || temp == '\n');
        ungetc(temp, fp);
        // Aging algorithm starts here
        clk++;
        // Check if page is in the memory
        for (i=0;i<page_count;i++){
            if (pages[i]->page_id == current_page){
                
                printf("HIT: %d\n", current_page);
                for (int j=0;j<page_count;j++){
                    printf("%d\n",pages[j]->page_id);
                }

                hit++;
                is_hit = 1;
                pages[i]->r = 1;
                break;
            }
        }
        // If it is not in the memory, replace
        if (!is_hit){
            miss++;
            page_to_replace = 0;
            for (i=0;i<page_count;i++){
                if (pages[i]->page_id == -1){
                    page_to_replace = i;
                    break;
                }
                if (pages[i]->cnt < pages[page_to_replace]->cnt){
                    page_to_replace = i;
                }
            }
            pages[page_to_replace]->page_id = current_page;
            // pages[page_to_replace]->r = 1;
            // pages[page_to_replace]->cnt = 0x80;
            pages[page_to_replace]->cnt = 0x0;
        }
        // Shift by 1 to right and add 1 to the beginning (left)        
        if (DEBUG) printf("Clock %d (%d)\n", clk, current_page);
        for (i=0;i<page_count;i++){
            // pages[i]->cnt = (pages[i]->cnt >> 1);
            pages[i]->cnt >>= 1;
            pages[i]->r = 0;
            if (current_page == pages[i]->page_id){
                pages[i]->cnt = pages[i]->cnt | 0x80;
                pages[i]->r = 1;
            }
            if (DEBUG){
                printf("%d -> ", pages[i]->page_id);
                print_bin(pages[i]->cnt);
                printf(" (%u)\n", pages[i]->cnt);
            }
        }
        is_hit = 0;
    }
    printf ("hits: %d\nmisses: %d\n", hit, miss);
    printf ("Hit rate: %f\n", (double)hit/(double)(hit+miss));
    printf ("Miss rate: %f\n", (double)miss/(double)(hit+miss));
    printf ("Ratio: %f\n", (double)hit/(double)(miss));
}