page_frames = 10
filename = "Lab 09 input.txt"
worst_case_file = "Lab 09 worst case.txt"
optimal_case_file = "Lab 09 optimal case.txt"

with open(filename, 'r') as fp:
    data = fp.readline()
    pages_str = data.strip().split(' ')
    # pages = [int(x) for x in pages_str]
    # pages = list(set(pages))
    pages = {}
    for x in pages_str:
        if int(x) in pages.keys():
            pages[int(x)] += 1
        else:
            pages[int(x)] = 1
    pages = sorted(pages.items(), key = lambda pair: pair[1])
    print(pages)
    print(len(pages))

    # Generating optimal sequence
    optimal = []
    for page, count in pages:
        optimal += [page for i in range(0,count) ]
    print(optimal)
    print(len(optimal))

    worst = []
    pages.reverse()
    next = [list(page) for page in pages]

    print(next)

    # Generating worst case sequence
    prev = []
    curr = []
    size = page_frames
    while True:
        idx = 0
        cnt = 0
        curr = []
        while (cnt < size and idx < len(next)):
            if next[idx][1] != 0 and prev.count(next[idx][0]) == 0 :
                cnt += 1
                next[idx][1] -= 1
                worst.append(next[idx][0])
                curr.append(next[idx][0])
            idx += 1
            # print(idx)
        prev = curr
        if cnt < size:
            while True:
                cnt = 0
                idx = 0
                while (cnt < size and idx < len(next)):
                    if next[idx][1] != 0:
                        cnt += 1
                        next[idx][1] -= 1
                        worst.append(next[idx][0])
                        # if next[idx][0] == 540:
                        #     print("Houston, we have problems :)")
                    idx += 1
                if cnt == 0:
                    break
            break
                
    # Checking worst case sequence
    next = dict(pages)
    for page in worst:
        next[page] -= 1
    for v in next.values():
        if v != 0:
            print("Worst case sequence is incorrect")

    print ('/////////////// WORST CASE BELOW //////////////////')
    print (worst)

    with open(worst_case_file, 'w') as file_to_write:
        file_to_write.write(' '.join([str(x) for x in worst]))

    with open(optimal_case_file, 'w') as file_to_write:
        file_to_write.write(' '.join([str(x) for x in optimal]))

        
